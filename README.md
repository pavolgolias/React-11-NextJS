## JS-11 - React example project - NextJS
This is an example React project covering multiple React features. Based on UDEMY course: https://www.udemy.com/react-the-complete-guide-incl-redux/.

**Examples in this project are focused mainly on NextJS and Server Side Rendering.**
